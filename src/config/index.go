package config

const (
	FormatDateSlashWithTime = "2006/01/02 15:04:05"
	FormatDateGroupMonth    = "200601"

	ParseQueryParamStartDate = "start_date"
	ParseQueryParamEndDate   = "end_date"
	ParseQueryParamStatus    = "status"
)
