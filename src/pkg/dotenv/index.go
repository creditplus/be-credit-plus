package pkg_dotenv

import (
	"os"

	"github.com/joho/godotenv"
)

type (
	PkgDotenv          struct{}
	PkgDotenvInterface interface {
		Load(path string) error
		Get(key string) string
	}
)

func New() PkgDotenvInterface {
	return &PkgDotenv{}
}

func (e *PkgDotenv) Load(path string) error {
	err := godotenv.Load(path)
	if err != nil {
		return err
	}
	return nil
}

func (e *PkgDotenv) Get(key string) string {
	return os.Getenv(key)
}
