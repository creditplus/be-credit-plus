package pkg_jwt

type ClaimTokenJWT struct {
	CustomerId string `json:"customer_id"`
}

type GenerateJWTTokenResponse struct {
	Token string
	Error error
}

type ValidateJWTTokenResponse struct {
	Data  *ClaimTokenJWT
	Error error
}
