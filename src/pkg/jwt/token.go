package pkg_jwt

import (
	"context"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

func (t *PkgJwt) GenerateJWTToken(ctx context.Context, data *ClaimTokenJWT, expired time.Duration) <-chan GenerateJWTTokenResponse {

	response := make(chan GenerateJWTTokenResponse)

	go func() {

		defer close(response)

		secretKey := []byte(t.TokenSecret)
		jwtToken := jwt.New(jwt.SigningMethodHS512)

		claims := jwtToken.Claims.(jwt.MapClaims)
		claims["exp"] = time.Now().Add(expired).Unix()
		claims["customer_id"] = data.CustomerId

		tokenString, err := jwtToken.SignedString(secretKey)
		if err != nil {
			response <- GenerateJWTTokenResponse{
				Error: err,
			}
			return
		}

		response <- GenerateJWTTokenResponse{
			Error: nil,
			Token: tokenString,
		}

	}()

	return response

}

func (t *PkgJwt) ValidateJWTToken(ctx context.Context, tokenString string) <-chan ValidateJWTTokenResponse {

	response := make(chan ValidateJWTTokenResponse)

	go func() {

		defer close(response)

		secretKey := []byte(t.TokenSecret)

		responseToken, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			}
			return secretKey, nil
		})
		if err != nil {
			response <- ValidateJWTTokenResponse{
				Error: err,
			}
			return
		}

		if !responseToken.Valid {
			response <- ValidateJWTTokenResponse{
				Error: fmt.Errorf("invalid token"),
			}
		}

		claims, ok := responseToken.Claims.(jwt.MapClaims)
		if !ok {
			response <- ValidateJWTTokenResponse{
				Error: fmt.Errorf("failed to get JWT claims"),
			}
			return
		}

		exp, ok := claims["exp"]
		if !ok {
			response <- ValidateJWTTokenResponse{
				Error: fmt.Errorf("failed to get exp claim"),
			}
			return
		}

		if time.Now().Unix() > int64(exp.(float64)) {
			response <- ValidateJWTTokenResponse{
				Error: fmt.Errorf("token has expired"),
			}
			return
		}

		dataClaim := ClaimTokenJWT{
			CustomerId: claims["customer_id"].(string),
		}

		response <- ValidateJWTTokenResponse{
			Data:  &dataClaim,
			Error: nil,
		}

	}()

	return response

}
