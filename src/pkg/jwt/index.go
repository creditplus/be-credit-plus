package pkg_jwt

import (
	"context"
	"time"
)

type PkgJwt struct {
	TokenSecret string
}

type PkgJwtTokenInterface interface {
	GenerateJWTToken(ctx context.Context, data *ClaimTokenJWT, expired time.Duration) <-chan GenerateJWTTokenResponse
	ValidateJWTToken(ctx context.Context, tokenString string) <-chan ValidateJWTTokenResponse
}

type PkgJwtInterface interface {
	PkgJwtTokenInterface
}

func New(tokenSecret string) PkgJwtInterface {
	return &PkgJwt{
		TokenSecret: tokenSecret,
	}
}
