package pkg_mysql

import "reflect"

func (p *PkgMysql) GetColumnNames(entity interface{}, tagName string, exclude ...string) (columns []string) {

	var (
		objVal     = reflect.ValueOf(entity).Elem()
		objType    = objVal.Type()
		mapExclude = make(map[string]bool, 0)
	)

	for _, itemExclude := range exclude {
		if _, found := mapExclude[itemExclude]; !found {
			mapExclude[itemExclude] = true
		}
	}

	for i := 0; i < objType.NumField(); i++ {
		col := objType.Field(i).Tag.Get(tagName)
		if _, found := mapExclude[col]; found {
			continue
		}
		if col != "" {
			columns = append(columns, col)
		}
	}

	return columns

}
