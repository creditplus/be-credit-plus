package pkg_mysql

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

type (
	PkgMysql struct {
		DatabaseName     string
		DatabaseServer   string
		DatabasePort     int
		DatabaseUser     string
		DatabasePassword string
		DatabaseMinPool  int
		DatabaseMaxPool  int
	}

	PkgMysqlToolsInterface interface {
		GetColumnNames(entity interface{}, tagName string, exclude ...string) (columns []string)
	}

	PkgMysqlConnectionInterface interface {
		CreateConnection() (*sql.DB, error)
		CreateStringConnection() string
	}

	PkgMysqlInterface interface {
		PkgMysqlToolsInterface
		PkgMysqlConnectionInterface
	}
)

func New(
	configMssqlDatabaseName string,
	configMssqlDatabaseServer string,
	configMssqlDatabasePort int,
	configMssqlDatabaseUser string,
	configMssqlDatabasePassword string,
	configMssqlDatabaseMinPool int,
	configMssqlDatabaseMaxPool int,
) PkgMysqlInterface {
	return &PkgMysql{
		DatabaseName:     configMssqlDatabaseName,
		DatabaseServer:   configMssqlDatabaseServer,
		DatabasePort:     configMssqlDatabasePort,
		DatabaseUser:     configMssqlDatabaseUser,
		DatabasePassword: configMssqlDatabasePassword,
		DatabaseMinPool:  configMssqlDatabaseMinPool,
		DatabaseMaxPool:  configMssqlDatabaseMaxPool,
	}
}

func NewConnection(
	configMssqlDatabaseName string,
	configMssqlDatabaseServer string,
	configMssqlDatabasePort int,
	configMssqlDatabaseUser string,
	configMssqlDatabasePassword string,
	configMssqlDatabaseMinPool int,
	configMssqlDatabaseMaxPool int,
) PkgMysqlConnectionInterface {
	return &PkgMysql{
		DatabaseName:     configMssqlDatabaseName,
		DatabaseServer:   configMssqlDatabaseServer,
		DatabasePort:     configMssqlDatabasePort,
		DatabaseUser:     configMssqlDatabaseUser,
		DatabasePassword: configMssqlDatabasePassword,
		DatabaseMinPool:  configMssqlDatabaseMinPool,
		DatabaseMaxPool:  configMssqlDatabaseMaxPool,
	}
}

func NewTools(
	configMssqlDatabaseName string,
	configMssqlDatabaseServer string,
	configMssqlDatabasePort int,
	configMssqlDatabaseUser string,
	configMssqlDatabasePassword string,
	configMssqlDatabaseMinPool int,
	configMssqlDatabaseMaxPool int,
) PkgMysqlToolsInterface {
	return &PkgMysql{
		DatabaseName:     configMssqlDatabaseName,
		DatabaseServer:   configMssqlDatabaseServer,
		DatabasePort:     configMssqlDatabasePort,
		DatabaseUser:     configMssqlDatabaseUser,
		DatabasePassword: configMssqlDatabasePassword,
		DatabaseMinPool:  configMssqlDatabaseMinPool,
		DatabaseMaxPool:  configMssqlDatabaseMaxPool,
	}
}
