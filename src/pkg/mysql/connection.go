package pkg_mysql

import (
	"database/sql"
	"fmt"
	"time"
)

func (p *PkgMysql) CreateStringConnection() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", p.DatabaseUser, p.DatabasePassword, p.DatabaseServer, p.DatabasePort, p.DatabaseName)
}

func (p *PkgMysql) CreateConnection() (*sql.DB, error) {
	database, err := sql.Open("mysql", p.CreateStringConnection())
	if err != nil {
		return nil, err
	}

	database.SetMaxIdleConns(p.DatabaseMinPool)
	database.SetMaxOpenConns(p.DatabaseMaxPool)
	database.SetConnMaxIdleTime(5 * time.Minute)
	database.SetConnMaxLifetime(60 * time.Minute)

	return database, nil

}
