package router_v1_transaction_credit

import (
	"github.com/gin-gonic/gin"
	handler_transaction_credit "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/handler/transaction_credit"
	middleware_creditplus "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/middleware"
)

func New(
	router *gin.RouterGroup,
	middleware middleware_creditplus.MiddlewareInterface,
	handler handler_transaction_credit.HandlerTransactionCreditInterface,
) {

	groupCustomer := router.Group("/transaction")
	groupCustomer.Use(middleware.JwtAuthMiddleware())

	groupCustomer.POST("/credit", handler.CreateOneTransactionCredit)

}
