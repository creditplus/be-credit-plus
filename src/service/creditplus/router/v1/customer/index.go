package router_v1_customer

import (
	"github.com/gin-gonic/gin"
	handler_customer "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/handler/customer"
	middleware_creditplus "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/middleware"
)

func New(
	router *gin.RouterGroup,
	middleware middleware_creditplus.MiddlewareInterface,
	handler handler_customer.HandlerCustomerInterface,
) {

	groupCustomer := router.Group("/customer")

	groupCustomer.POST("/login", handler.Login)
	groupCustomer.POST("/register", handler.Register)

}
