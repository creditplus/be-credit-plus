package config_creditplus

type (
	ConfigServiceCreditPlus struct {
		ConfigServicePort string
		ConfigServiceMode string
	}
)
