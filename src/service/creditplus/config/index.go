package config_creditplus

type (
	ConfigCredtiPlus struct {
		ConfigDatabaseCreditPlus
		ConfigServiceCreditPlus
		ConfigAuthorizationCreditPlus
	}
)
