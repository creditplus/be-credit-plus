package config_creditplus

type (
	ConfigDatabaseCreditPlus struct {
		ConfigMysqlDatabaseName     string
		ConfigMysqlDatabaseServer   string
		ConfigMysqlDatabasePort     int
		ConfigMysqlDatabaseUser     string
		ConfigMysqlDatabasePassword string
		ConfigMysqlDatabaseMinPool  int
		ConfigMysqlDatabaseMaxPool  int
	}
)
