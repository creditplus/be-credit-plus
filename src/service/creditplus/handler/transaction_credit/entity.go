package handler_transaction_credit

type (
	Transaction struct {
		Tenor        int     `json:"tenor"`
		Otr          float64 `json:"otr"`
		AdminFee     float64 `json:"admin_fee"`
		TotalCicilan float64 `json:"total_cicilan"`
		TotalBunga   float64 `json:"total_bunga"`
		NameAsset    string  `json:"name_asset"`
	}

	CreateOneTransactionCreditRequest struct {
		Tenor      int     `json:"tenor" binding:"required,gte=1"`
		PriceAsset float64 `json:"price_asset" binding:"required,gte=1"`
		NameAsset  string  `json:"name_asset" binding:"required"`
	}

	CreateOneTransactionCreditResponse struct {
		Data []Transaction `json:"data"`
	}
)
