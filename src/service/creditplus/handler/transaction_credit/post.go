package handler_transaction_credit

import (
	"net/http"

	"github.com/gin-gonic/gin"
	config_creditplus "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/config"
	usecase_transaction_credit "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/usecase/transaction_credit"
)

func (h *HandlerTransactionCredit) CreateOneTransactionCredit(c *gin.Context) {

	var (
		err     error
		request CreateOneTransactionCreditRequest
	)

	if err = c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"code": http.StatusBadRequest, "message": err.Error()})
		return
	}

	customerId := c.MustGet(config_creditplus.AuthorizationCustomerId).(string)

	_, err = h.UsecaseTransactionCredit.CreateNewTransactionCredit(c.Request.Context(), &usecase_transaction_credit.CreateNewTransactionCreditRequest{
		CustomerId: customerId,
		Tenor:      request.Tenor,
		PriceAsset: request.PriceAsset,
		NameAsset:  request.NameAsset,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code": http.StatusInternalServerError, "message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"code": http.StatusOK, "message": "Success create new transaction"})

}
