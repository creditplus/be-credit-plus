package handler_transaction_credit

import (
	"github.com/gin-gonic/gin"
	usecase_transaction_credit "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/usecase/transaction_credit"
)

type (
	HandlerTransactionCredit struct {
		UsecaseTransactionCredit usecase_transaction_credit.UsecaseTransactionCreditInterface
	}
	HandlerTransactionCreditPostInterface interface {
		CreateOneTransactionCredit(c *gin.Context)
	}
	HandlerTransactionCreditInterface interface {
		HandlerTransactionCreditPostInterface
	}
)

func New(
	usecaseTransactionCredit usecase_transaction_credit.UsecaseTransactionCreditInterface,
) HandlerTransactionCreditInterface {
	return &HandlerTransactionCredit{
		UsecaseTransactionCredit: usecaseTransactionCredit,
	}
}
