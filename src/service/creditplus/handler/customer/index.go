package handler_customer

import (
	"github.com/gin-gonic/gin"
	usecase_customer "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/usecase/customer"
)

type (
	HandlerCustomer struct {
		UsecaseCustomer usecase_customer.UsecaseCustomerInterface
	}

	HandlerCustomerAuthInteface interface {
		Login(c *gin.Context)
		Register(c *gin.Context)
	}

	HandlerCustomerInterface interface {
		HandlerCustomerAuthInteface
	}
)

func New(
	usecaseCustomer usecase_customer.UsecaseCustomerInterface,
) HandlerCustomerInterface {
	return &HandlerCustomer{
		UsecaseCustomer: usecaseCustomer,
	}
}
