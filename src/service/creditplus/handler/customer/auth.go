package handler_customer

import (
	"net/http"

	"github.com/gin-gonic/gin"
	usecase_customer "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/usecase/customer"
)

func (h *HandlerCustomer) Login(c *gin.Context) {

	var (
		err     error
		request LoginRequest
	)

	if err = c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"code": http.StatusBadRequest, "message": err.Error()})
		return
	}

	responseLogin, err := h.UsecaseCustomer.Login(
		c.Request.Context(),
		&usecase_customer.LoginRequest{
			Username: request.Username,
			Password: request.Password,
		},
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code": http.StatusInternalServerError, "message": err.Error()})
		return
	}

	response := new(LoginResponse)
	response.Token = responseLogin.Token
	response.Username = responseLogin.Username

	c.JSON(http.StatusOK, gin.H{"code": http.StatusOK, "message": "Success login", "data": response})

}

func (h *HandlerCustomer) Register(c *gin.Context) {

	var (
		err     error
		request RegisterRequest
	)

	if err = c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"code": http.StatusBadRequest, "message": err.Error()})
		return
	}

	listCustomerLimit := make([]usecase_customer.CustomerLimit, 0)
	for _, itemOfCustomerLimit := range request.DataCustomerLimit {
		listCustomerLimit = append(listCustomerLimit, usecase_customer.CustomerLimit{
			Tenor: itemOfCustomerLimit.Tenor,
			Limit: itemOfCustomerLimit.Limit,
		})
	}

	_, err = h.UsecaseCustomer.Register(
		c.Request.Context(),
		&usecase_customer.RegisterRequest{
			Username:          request.Username,
			Password:          request.Password,
			NIK:               request.NIK,
			FullName:          request.FullName,
			LegalName:         request.LegalName,
			PlaceBirth:        request.PlaceBirth,
			DateBirth:         request.DateBirth,
			Salary:            request.Salary,
			PhotoURLKTP:       request.PhotoURLKTP,
			PhotoURLSelfie:    request.PhotoURLSelfie,
			DataCustomerLimit: listCustomerLimit,
		},
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"code": http.StatusInternalServerError, "message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"code": http.StatusOK, "message": "Success register"})

}
