package handler_customer

type (
	CustomerLimit struct {
		Tenor int     `json:"tenor" binding:"required"`
		Limit float64 `json:"limit" binding:"required"`
	}

	RegisterRequest struct {
		Username          string          `json:"username" binding:"required"`
		Password          string          `json:"password" binding:"required"`
		NIK               string          `json:"nik" binding:"required"`
		FullName          string          `json:"full_name" binding:"required"`
		LegalName         string          `json:"legal_name" binding:"required"`
		PlaceBirth        string          `json:"place_birth" binding:"required"`
		DateBirth         string          `json:"date_birth" binding:"required"`
		Salary            float64         `json:"salary" binding:"required"`
		PhotoURLKTP       string          `json:"photo_url_ktp" binding:"required"`
		PhotoURLSelfie    string          `json:"photo_url_selfie" binding:"required"`
		DataCustomerLimit []CustomerLimit `json:"customer_limit" binding:"required"`
	}

	RegisterResponse struct {
	}

	LoginRequest struct {
		Username string `json:"username" binding:"required"`
		Password string `json:"password" binding:"required"`
	}

	LoginResponse struct {
		Username string `json:"username"`
		Token    string `json:"token"`
	}
)
