package model_mysql_customer_login

import (
	"context"
	"database/sql"

	pkg_mysql "gitlab.com/creditplus/be-credit-plus/src/pkg/mysql"
)

type (
	ModelMysqlCustomerLogin struct {
		PkgMysqlTools   pkg_mysql.PkgMysqlToolsInterface
		ConnectionMysql *sql.DB
	}
	ModelMysqlCustomerLoginGetInterface interface {
		FindOne(ctx context.Context, request *FindOneRequest) (response FindOneResponse, err error)
	}

	ModelMysqlCustomerLoginPostInterface interface {
		CreateBulk(ctx context.Context, request *CreateBulkRequest) (response CreateBulkResponse, err error)
	}
	ModelMysqlCustomerLoginInterface interface {
		ModelMysqlCustomerLoginGetInterface
		ModelMysqlCustomerLoginPostInterface
	}
)

func New(
	pkgMysqlTools pkg_mysql.PkgMysqlToolsInterface,
	connectionMysql *sql.DB,
) ModelMysqlCustomerLoginInterface {
	return &ModelMysqlCustomerLogin{
		PkgMysqlTools:   pkgMysqlTools,
		ConnectionMysql: connectionMysql,
	}
}
