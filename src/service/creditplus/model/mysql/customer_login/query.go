package model_mysql_customer_login

const (
	QueryFindOne = `
		SELECT 
			%s
		FROM
			customers_login
		WHERE
			%s
	`

	QueryInsertMany = `
		INSERT INTO customers_login (
			%s
		) 
		VALUES 
			%s		
	`
)
