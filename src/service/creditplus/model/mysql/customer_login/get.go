package model_mysql_customer_login

import (
	"context"
	"fmt"
	"strings"
)

func (m *ModelMysqlCustomerLogin) FindOne(ctx context.Context, request *FindOneRequest) (response FindOneResponse, err error) {

	var (
		argsString = make([]string, 0)
		argsValue  = make([]interface{}, 0)
	)

	if request.Username != "" {
		argsString = append(argsString, "username = ?")
		argsValue = append(argsValue, request.Username)
	}

	stmt, err := m.ConnectionMysql.PrepareContext(
		ctx,
		fmt.Sprintf(
			QueryFindOne,
			strings.Join(m.PkgMysqlTools.GetColumnNames(&CustomersLogin{}, "db"), ","),
			strings.Join(argsString, "AND"),
		),
	)
	if err != nil {
		return response, err
	}

	defer stmt.Close()

	err = stmt.QueryRowContext(ctx, argsValue...).Scan(
		&response.Data.Id,
		&response.Data.CustomerId,
		&response.Data.Username,
		&response.Data.Password,
	)
	if err != nil {
		return response, err
	}

	return response, err

}
