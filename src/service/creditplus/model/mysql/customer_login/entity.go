package model_mysql_customer_login

import "database/sql"

type (
	CustomersLogin struct {
		Id         string `db:"id"`
		CustomerId string `db:"customer_id"`
		Username   string `db:"username"`
		Password   string `db:"password"`
	}

	FindOneRequest struct {
		Username string
	}

	FindOneResponse struct {
		Data CustomersLogin
	}

	CustomerLoginModeCreate struct {
		CustomerId string `db:"customer_id"`
		Username   string `db:"username"`
		Password   string `db:"password"`
	}

	CreateBulkRequest struct {
		Data        []CustomerLoginModeCreate
		Transaction *sql.Tx
	}

	CreateBulkResponse struct {
	}
)
