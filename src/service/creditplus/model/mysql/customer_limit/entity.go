package model_mysql_customer_limit

import "database/sql"

type (
	CustomerLimit struct {
		Id         string  `db:"id"`
		CustomerId string  `db:"customer_id"`
		Tenor      int     `db:"tenor"`
		Limit      float64 `db:"limit_value"`
	}

	FindOneRequest struct {
		Username string
	}

	FindOneResponse struct {
		Data CustomerLimit
	}

	CustomerLimitModeCreate struct {
		CustomerId string  `db:"customer_id"`
		Tenor      int     `db:"tenor"`
		Limit      float64 `db:"limit_value"`
	}

	CreateBulkRequest struct {
		Data        []CustomerLimitModeCreate
		Transaction *sql.Tx
	}

	CreateBulkResponse struct {
	}
)
