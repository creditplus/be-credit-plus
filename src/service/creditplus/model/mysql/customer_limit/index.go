package model_mysql_customer_limit

import (
	"context"
	"database/sql"

	pkg_mysql "gitlab.com/creditplus/be-credit-plus/src/pkg/mysql"
)

type (
	ModelMysqlCustomerLimit struct {
		PkgMysqlTools   pkg_mysql.PkgMysqlToolsInterface
		ConnectionMysql *sql.DB
	}
	ModelMysqlCustomerLimitPostInterface interface {
		CreateBulk(ctx context.Context, request *CreateBulkRequest) (response CreateBulkResponse, err error)
	}
	ModelMysqlCustomerLimitInterface interface {
		ModelMysqlCustomerLimitPostInterface
	}
)

func New(
	pkgMysqlTools pkg_mysql.PkgMysqlToolsInterface,
	connectionMysql *sql.DB,
) ModelMysqlCustomerLimitInterface {
	return &ModelMysqlCustomerLimit{
		PkgMysqlTools:   pkgMysqlTools,
		ConnectionMysql: connectionMysql,
	}
}
