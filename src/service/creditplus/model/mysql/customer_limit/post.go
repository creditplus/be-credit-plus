package model_mysql_customer_limit

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

func (m *ModelMysqlCustomerLimit) CreateBulk(ctx context.Context, request *CreateBulkRequest) (response CreateBulkResponse, err error) {

	var (
		argsString = make([]string, 0)
		argsValue  = make([]interface{}, 0)

		columnInsert = m.PkgMysqlTools.GetColumnNames(&CustomerLimitModeCreate{}, "db")
	)

	if len(request.Data) == 0 {
		return response, errors.New("data leng must greater 0")
	}

	for _, itemOfData := range request.Data {
		argsString = append(argsString, "(?,?,?)")
		argsValue = append(argsValue, itemOfData.CustomerId, itemOfData.Tenor, itemOfData.Limit)
	}

	stmt, err := request.Transaction.PrepareContext(
		ctx,
		fmt.Sprintf(
			QueryInsertMany,
			strings.Join(columnInsert, ","),
			strings.Join(argsString, ","),
		),
	)
	if err != nil {
		return response, err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(
		ctx,
		argsValue...,
	)
	if err != nil {
		return response, err
	}

	return response, err

}
