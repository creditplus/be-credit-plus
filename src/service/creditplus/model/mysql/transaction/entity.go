package model_mysql_transaction

import "database/sql"

type (
	Transaction struct {
		Id           string  `db:"id"`
		CustomerId   string  `db:"customer_id"`
		Tenor        int     `db:"tenor"`
		Otr          float64 `db:"otr"`
		AdminFee     float64 `db:"admin_fee"`
		TotalCicilan float64 `db:"total_cicilan"`
		TotalBunga   float64 `db:"total_bunga"`
		NameAsset    string  `db:"name_asset"`
	}

	CreateBulkRequest struct {
		Data        []Transaction
		Transaction *sql.Tx
	}

	CreateBulkResponse struct {
	}
)
