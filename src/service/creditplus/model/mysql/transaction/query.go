package model_mysql_transaction

const (
	QueryInsertBulk = `
		INSERT INTO transaction (
			%s
		) 
		VALUES 
			%s;
	`
)
