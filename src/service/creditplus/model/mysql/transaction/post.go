package model_mysql_transaction

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

func (m *ModelMysqlTransaction) CreateBulk(ctx context.Context, request *CreateBulkRequest) (response CreateBulkResponse, err error) {

	var (
		argsString = make([]string, 0)
		argsValue  = make([]interface{}, 0)

		columnInsert = m.PkgMysqlTools.GetColumnNames(&Transaction{}, "db", "id")
	)

	for _, item := range request.Data {
		argsString = append(argsString, "(?, ?, ?, ?, ?, ?, ?)")
		argsValue = append(argsValue, item.CustomerId)
		argsValue = append(argsValue, item.Tenor)
		argsValue = append(argsValue, item.Otr)
		argsValue = append(argsValue, item.AdminFee)
		argsValue = append(argsValue, item.TotalCicilan)
		argsValue = append(argsValue, item.TotalBunga)
		argsValue = append(argsValue, item.NameAsset)
	}

	if request.Transaction == nil {
		return response, errors.New("transaction required [model]")
	}

	stmt, err := request.Transaction.PrepareContext(
		ctx,
		fmt.Sprintf(
			QueryInsertBulk,
			strings.Join(columnInsert, ","),
			strings.Join(argsString, ","),
		),
	)
	if err != nil {
		return response, err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, argsValue...)
	if err != nil {
		return response, err
	}

	return response, err

}
