package model_mysql_transaction

import (
	"context"
	"database/sql"

	pkg_mysql "gitlab.com/creditplus/be-credit-plus/src/pkg/mysql"
)

type (
	ModelMysqlTransaction struct {
		PkgMysqlTools   pkg_mysql.PkgMysqlToolsInterface
		ConnectionMysql *sql.DB
	}
	ModelMyssqlTransactionPostInterface interface {
		CreateBulk(ctx context.Context, request *CreateBulkRequest) (response CreateBulkResponse, err error)
	}
	ModelMysqlTransactionInterface interface {
		ModelMyssqlTransactionPostInterface
	}
)

func New(
	pkgMysqlTools pkg_mysql.PkgMysqlToolsInterface,
	connectionMysql *sql.DB,
) ModelMysqlTransactionInterface {
	return &ModelMysqlTransaction{
		PkgMysqlTools:   pkgMysqlTools,
		ConnectionMysql: connectionMysql,
	}
}
