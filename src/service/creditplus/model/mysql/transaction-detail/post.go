package model_mysql_transaction_detail

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

func (m *ModelMysqlTransactionDetail) CreateBulk(ctx context.Context, request *CreateBulkRequest) (response CreateBulkResponse, err error) {

	var (
		argsString = make([]string, 0)
		argsValue  = make([]interface{}, 0)

		columnInsert = m.PkgMysqlTools.GetColumnNames(&TransactionDetail{}, "db", "id")
	)

	i := 0
	for _, item := range request.Data {
		argsString = append(argsString, "(?, ?, ?)")
		argsValue = append(argsValue, item.TransactionId)
		argsValue = append(argsValue, item.Cicilan)
		argsValue = append(argsValue, item.Bunga)
		i += 3
	}

	if request.Transaction == nil {
		return response, errors.New("transaction required [model]")
	}

	stmt, err := request.Transaction.PrepareContext(
		ctx,
		fmt.Sprintf(
			QueryInsertBulk,
			strings.Join(columnInsert, ","),
			strings.Join(argsString, ","),
		),
	)
	if err != nil {
		return response, err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, argsValue...)
	if err != nil {
		return response, err
	}

	return response, err

}
