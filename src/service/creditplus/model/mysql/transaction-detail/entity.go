package model_mysql_transaction_detail

import "database/sql"

type (
	TransactionDetail struct {
		Id            string  `db:"id"`
		TransactionId string  `db:"transaction_id"`
		Cicilan       float64 `db:"cicilan"`
		Bunga         float64 `db:"bunga"`
	}

	CreateBulkRequest struct {
		Data        []TransactionDetail
		Transaction *sql.Tx
	}

	CreateBulkResponse struct {
	}
)
