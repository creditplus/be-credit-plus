package model_mysql_transaction_detail

import (
	"context"
	"database/sql"

	pkg_mysql "gitlab.com/creditplus/be-credit-plus/src/pkg/mysql"
)

type (
	ModelMysqlTransactionDetail struct {
		PkgMysqlTools   pkg_mysql.PkgMysqlToolsInterface
		ConnectionMysql *sql.DB
	}
	ModelMyssqlTransactionDetailPostInterface interface {
		CreateBulk(ctx context.Context, request *CreateBulkRequest) (response CreateBulkResponse, err error)
	}
	ModelMysqlTransactionDetailInterface interface {
		ModelMyssqlTransactionDetailPostInterface
	}
)

func New(
	pkgMysqlTools pkg_mysql.PkgMysqlToolsInterface,
	connectionMysql *sql.DB,
) ModelMysqlTransactionDetailInterface {
	return &ModelMysqlTransactionDetail{
		PkgMysqlTools:   pkgMysqlTools,
		ConnectionMysql: connectionMysql,
	}
}
