package model_mysql_customer

const (
	QueryFindWithId = `
		SELECT 
			%s
		FROM customers cstm
		JOIN customers_limit cstml 
			ON 
				cstm.id = cstml.customer_id
		WHERE
			cstm.id = %s
		FOR UPDATE;
	`

	QueryFindAll = `
		SELECT
			%s
		FROM customers
		WHERE
			%s;
	`

	QueryUpdateLimit = `
		UPDATE customers_limit
		SET
			%s
		WHERE 
			id = ?
			AND 
			tenor = ?				
	`

	QueryInsertMany = `
		INSERT INTO customers (
			%s
		) 
		VALUES 
			%s			
	`
)
