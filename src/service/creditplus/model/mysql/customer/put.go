package model_mysql_customer

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

func (m *ModelMysqlCustomer) UpdateLimit(ctx context.Context, request *UpdateLimitRequest) (response UpdateLimitResponse, err error) {

	var (
		argsString = make([]string, 0)
		argsValue  = make([]interface{}, 0)
	)

	if request.Limit < 0 {
		return response, errors.New("limit must higher than 0")
	}

	argsString = append(argsString, "limit_value = ?")
	argsValue = append(argsValue, request.Limit, request.CustomerLimitId, request.Tenor)

	stmt, err := request.Transaction.PrepareContext(
		ctx,
		fmt.Sprintf(
			QueryUpdateLimit,
			strings.Join(argsString, ","),
		),
	)
	if err != nil {
		return response, err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(
		ctx,
		argsValue...,
	)
	if err != nil {
		return response, err
	}

	return response, err

}
