package model_mysql_customer

import (
	"context"
	"database/sql"

	pkg_mysql "gitlab.com/creditplus/be-credit-plus/src/pkg/mysql"
)

type (
	ModelMysqlCustomer struct {
		PkgMysqlTools   pkg_mysql.PkgMysqlToolsInterface
		ConnectionMysql *sql.DB
	}
	ModelMysqlCustomerGetInterface interface {
		FindCustomerWithJoinLimit(ctx context.Context, request *FindCustomerWithJoinLimitRequest) (response FindCustomerWithJoinLimitResponse, err error)
		FindAll(ctx context.Context, request *FindAllRequest) (response FindAllResponse, err error)
	}
	ModelMysqlCustomerPutInterface interface {
		UpdateLimit(ctx context.Context, request *UpdateLimitRequest) (response UpdateLimitResponse, err error)
	}

	ModelMysqlCustomerPostInterface interface {
		CreateBulk(ctx context.Context, request *CreateBulkRequest) (response CreateBulkResponse, err error)
	}
	ModelMysqlCustomerInterface interface {
		ModelMysqlCustomerGetInterface
		ModelMysqlCustomerPutInterface
		ModelMysqlCustomerPostInterface
	}
)

func New(
	pkgMysqlTools pkg_mysql.PkgMysqlToolsInterface,
	connectionMysql *sql.DB,
) ModelMysqlCustomerInterface {
	return &ModelMysqlCustomer{
		PkgMysqlTools:   pkgMysqlTools,
		ConnectionMysql: connectionMysql,
	}
}
