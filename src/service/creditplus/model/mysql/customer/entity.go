package model_mysql_customer

import "database/sql"

type (
	Customer struct {
		Id             string  `db:"id" db_with_limit:"cstm.id"`
		NIK            string  `db:"nik" db_with_limit:"cstm.nik"`
		FullName       string  `db:"full_name" db_with_limit:"cstm.full_name"`
		LegalName      string  `db:"legal_name" db_with_limit:"cstm.legal_name"`
		PlaceBirth     string  `db:"place_birth" db_with_limit:"cstm.place_birth"`
		DateBirth      string  `db:"date_birth" db_with_limit:"cstm.date_birth"`
		Salary         float64 `db:"salary" db_with_limit:"cstm.salary"`
		PhotoURLKTP    string  `db:"photo_url_ktp" db_with_limit:"cstm.photo_url_ktp"`
		PhotoURLSelfie string  `db:"photo_url_selfie" db_with_limit:"cstm.photo_url_selfie"`
	}

	CustomerWithLimit struct {
		Id              string  `db:"id" db_with_limit:"cstm.id"`
		NIK             string  `db:"nik" db_with_limit:"cstm.nik"`
		FullName        string  `db:"full_name" db_with_limit:"cstm.full_name"`
		LegalName       string  `db:"legal_name" db_with_limit:"cstm.legal_name"`
		PlaceBirth      string  `db:"place_birth" db_with_limit:"cstm.place_birth"`
		DateBirth       string  `db:"date_birth" db_with_limit:"cstm.date_birth"`
		Salary          float64 `db:"salary" db_with_limit:"cstm.salary"`
		PhotoURLKTP     string  `db:"photo_url_ktp" db_with_limit:"cstm.photo_url_ktp"`
		PhotoURLSelfie  string  `db:"photo_url_selfie" db_with_limit:"cstm.photo_url_selfie"`
		CustomerLimitId string  `db_with_limit:"cstml.id"`
		Tenor           int     `db_with_limit:"cstml.tenor"`
		LimitValue      float64 `db_with_limit:"cstml.limit_value"`
	}

	FindCustomerWithJoinLimitRequest struct {
		Id          string
		Transaction *sql.Tx
	}

	FindCustomerWithJoinLimitResponse struct {
		Data []CustomerWithLimit
	}

	FindAllRequest struct {
		NIK string
	}

	FindAllResponse struct {
		Data []Customer
	}

	UpdateLimitRequest struct {
		CustomerLimitId string
		Tenor           int
		Limit           float64
		Transaction     *sql.Tx
	}

	UpdateLimitResponse struct {
	}

	CustomerModeCreate struct {
		NIK            string  `db:"nik"`
		FullName       string  `db:"full_name"`
		LegalName      string  `db:"legal_name"`
		PlaceBirth     string  `db:"place_birth"`
		DateBirth      string  `db:"date_birth"`
		Salary         float64 `db:"salary"`
		PhotoURLKTP    string  `db:"photo_url_ktp"`
		PhotoURLSelfie string  `db:"photo_url_selfie"`
	}

	CreateBulkRequest struct {
		Data        []CustomerModeCreate
		Transaction *sql.Tx
	}

	CreateBulkResponse struct {
		Id []string
	}
)
