package model_mysql_customer

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

func (m *ModelMysqlCustomer) FindCustomerWithJoinLimit(ctx context.Context, request *FindCustomerWithJoinLimitRequest) (response FindCustomerWithJoinLimitResponse, err error) {

	var (
		argsValue   = make([]interface{}, 0)
		selectQuery string
	)

	if request.Id == "" {
		return response, errors.New("id is required [model]")
	}

	selectQuery = fmt.Sprintf(
		QueryFindWithId,
		strings.Join(m.PkgMysqlTools.GetColumnNames(&CustomerWithLimit{}, "db_with_limit"), ","),
		"?",
	)

	argsValue = append(argsValue, request.Id)

	if request.Transaction == nil {
		return response, errors.New("transaction required [model]")
	}

	stmt, err := request.Transaction.PrepareContext(
		ctx,
		selectQuery,
	)
	if err != nil {
		return response, err
	}

	defer stmt.Close()

	rows, err := stmt.QueryContext(
		ctx,
		argsValue...,
	)
	if err != nil {
		return response, err
	}

	defer rows.Close()

	for rows.Next() {
		customer := new(CustomerWithLimit)
		err = rows.Scan(
			&customer.Id,
			&customer.NIK,
			&customer.FullName,
			&customer.LegalName,
			&customer.PlaceBirth,
			&customer.DateBirth,
			&customer.Salary,
			&customer.PhotoURLKTP,
			&customer.PhotoURLSelfie,
			&customer.CustomerLimitId,
			&customer.Tenor,
			&customer.LimitValue,
		)
		if err != nil {
			return response, err
		}
		response.Data = append(response.Data, *customer)
	}

	return response, err

}

func (m *ModelMysqlCustomer) FindAll(ctx context.Context, request *FindAllRequest) (response FindAllResponse, err error) {

	var (
		argsValue   = make([]interface{}, 0)
		argsString  = make([]string, 0)
		columns     = m.PkgMysqlTools.GetColumnNames(&Customer{}, "db")
		selectQuery string
	)

	if request.NIK != "" {
		argsString = append(argsString, "nik = ?")
		argsValue = append(argsValue, request.NIK)
	}

	selectQuery = fmt.Sprintf(
		QueryFindAll,
		strings.Join(columns, ","),
		strings.Join(argsString, "AND"),
	)

	stmt, err := m.ConnectionMysql.PrepareContext(
		ctx,
		selectQuery,
	)
	if err != nil {
		return response, err
	}

	defer stmt.Close()

	rows, err := stmt.QueryContext(
		ctx,
		argsValue...,
	)
	if err != nil {
		return response, err
	}

	defer rows.Close()

	for rows.Next() {
		customer := new(Customer)
		err = rows.Scan(
			&customer.Id,
			&customer.NIK,
			&customer.FullName,
			&customer.LegalName,
			&customer.PlaceBirth,
			&customer.DateBirth,
			&customer.Salary,
			&customer.PhotoURLKTP,
			&customer.PhotoURLSelfie,
		)
		if err != nil {
			return response, err
		}
		response.Data = append(response.Data, *customer)
	}

	return response, err

}
