package model_mysql_customer

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

func (m *ModelMysqlCustomer) CreateBulk(ctx context.Context, request *CreateBulkRequest) (response CreateBulkResponse, err error) {

	var (
		argsString    = make([]string, 0)
		argsValue     = make([]interface{}, 0)
		columnsInsert = m.PkgMysqlTools.GetColumnNames(&CustomerModeCreate{}, "db")
	)

	if len(request.Data) == 0 {
		return response, errors.New("data leng must greater then 0")
	}

	for _, itemOfData := range request.Data {
		argsString = append(argsString, "(?,?,?,?,?,?,?,?)")
		argsValue = append(
			argsValue,
			itemOfData.NIK,
			itemOfData.FullName,
			itemOfData.LegalName,
			itemOfData.PlaceBirth,
			itemOfData.DateBirth,
			itemOfData.Salary,
			itemOfData.PhotoURLKTP,
			itemOfData.PhotoURLSelfie,
		)
	}

	stmt, err := request.Transaction.PrepareContext(
		ctx,
		fmt.Sprintf(
			QueryInsertMany,
			strings.Join(columnsInsert, ","),
			strings.Join(argsString, ","),
		),
	)
	if err != nil {
		return response, err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, argsValue...)
	if err != nil {
		return response, err
	}

	return response, err

}
