package middleware_creditplus

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	config_creditplus "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/config"
)

func (m *Middleware) JwtAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		tokenHeader := c.Request.Header["Authorization"][0]
		tokenValue := strings.Split(tokenHeader, " ")[1]

		responsevalidateToken := <-m.PkgJwt.ValidateJWTToken(c.Request.Context(), tokenValue)

		if responsevalidateToken.Error != nil {
			c.JSON(http.StatusUnauthorized, gin.H{
				"code":    http.StatusUnauthorized,
				"message": responsevalidateToken.Error.Error(),
			})
			c.Abort()
			return
		}

		c.Set(config_creditplus.AuthorizationCustomerId, responsevalidateToken.Data.CustomerId)

		c.Next()

	}
}
