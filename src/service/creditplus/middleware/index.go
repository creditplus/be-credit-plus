package middleware_creditplus

import (
	"github.com/gin-gonic/gin"
	pkg_jwt "gitlab.com/creditplus/be-credit-plus/src/pkg/jwt"
)

type (
	Middleware struct {
		PkgJwt pkg_jwt.PkgJwtInterface
	}

	MiddlewareInterface interface {
		JwtAuthMiddleware() gin.HandlerFunc
	}
)

func New(
	pkgJwt pkg_jwt.PkgJwtInterface,
) MiddlewareInterface {
	return &Middleware{
		PkgJwt: pkgJwt,
	}
}
