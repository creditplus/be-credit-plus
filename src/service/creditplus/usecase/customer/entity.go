package usecase_customer

type (
	CustomerLimit struct {
		Tenor int     `json:"tenor"`
		Limit float64 `json:"limit"`
	}

	RegisterRequest struct {
		Username          string          `json:"username"`
		Password          string          `json:"password"`
		NIK               string          `json:"nik"`
		FullName          string          `json:"full_name"`
		LegalName         string          `json:"legal_name"`
		PlaceBirth        string          `json:"place_birth"`
		DateBirth         string          `json:"date_birth"`
		Salary            float64         `json:"salary"`
		PhotoURLKTP       string          `json:"photo_url_ktp"`
		PhotoURLSelfie    string          `json:"photo_url_selfie"`
		DataCustomerLimit []CustomerLimit `json:"customer_limit"`
	}

	RegisterResponse struct {
	}

	RegisterChannelInsertLimitAndAuthRequest struct {
		CustomerId        string
		DataCustomerLimit []CustomerLimit
	}

	RegisterChannelInsertLimitAndAuthResponse struct {
		Error error
	}

	LoginRequest struct {
		Username string
		Password string
	}

	LoginResponse struct {
		Username string
		Token    string
	}
)
