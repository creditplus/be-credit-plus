package usecase_customer

import (
	"context"
	"sync"
	"time"

	pkg_jwt "gitlab.com/creditplus/be-credit-plus/src/pkg/jwt"
	model_mysql_customer "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/customer"
	model_mysql_customer_limit "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/customer_limit"
	model_mysql_customer_login "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/customer_login"
)

func (u *UsecaseCustomer) Login(ctx context.Context, request *LoginRequest) (response LoginResponse, err error) {

	resCustomerLogin, err := u.ModelMysqlCustomerLogin.FindOne(
		ctx,
		&model_mysql_customer_login.FindOneRequest{
			Username: request.Username,
		},
	)
	if err != nil {
		return response, err
	}

	responseGenerateToken := <-u.PkgJwt.GenerateJWTToken(
		ctx,
		&pkg_jwt.ClaimTokenJWT{
			CustomerId: resCustomerLogin.Data.CustomerId,
		},
		time.Hour*24,
	)
	if responseGenerateToken.Error != nil {
		return response, err
	}

	response.Username = resCustomerLogin.Data.Username
	response.Token = responseGenerateToken.Token

	return response, err

}

func (u *UsecaseCustomer) Register(ctx context.Context, request *RegisterRequest) (response RegisterResponse, err error) {

	transactionSql, err := u.ConnectionMysql.Begin()
	if err != nil {
		return response, err
	}

	customerModeCreate := new(model_mysql_customer.CustomerModeCreate)

	customerModeCreate.NIK = request.NIK
	customerModeCreate.FullName = request.FullName
	customerModeCreate.LegalName = request.LegalName
	customerModeCreate.PlaceBirth = request.PlaceBirth
	customerModeCreate.DateBirth = request.DateBirth
	customerModeCreate.Salary = request.Salary
	customerModeCreate.PhotoURLKTP = request.PhotoURLKTP
	customerModeCreate.PhotoURLSelfie = request.PhotoURLSelfie

	_, err = u.ModelMysqlCustomer.CreateBulk(ctx, &model_mysql_customer.CreateBulkRequest{
		Transaction: transactionSql,
		Data: []model_mysql_customer.CustomerModeCreate{
			*customerModeCreate,
		},
	})
	if err != nil {
		transactionSql.Rollback()
		return response, err
	}

	transactionSql.Commit()

	responseFindAll, err := u.ModelMysqlCustomer.FindAll(ctx, &model_mysql_customer.FindAllRequest{
		NIK: request.NIK,
	})
	if err != nil {
		return response, err
	}

	var (
		customerId       = responseFindAll.Data[0].Id
		responseChan     = make(chan RegisterChannelInsertLimitAndAuthResponse, 2)
		targetGoroutines = 2
		wg               sync.WaitGroup
	)

	transactionSql, err = u.ConnectionMysql.Begin()
	if err != nil {
		return response, err
	}

	defer close(responseChan)

	wg.Add(2)

	go func(wg *sync.WaitGroup, requestChannel *RegisterChannelInsertLimitAndAuthRequest, responseChannel chan<- RegisterChannelInsertLimitAndAuthResponse) {

		defer wg.Done()

		requestCustomerLogin := new(model_mysql_customer_login.CustomerLoginModeCreate)
		requestCustomerLogin.CustomerId = requestChannel.CustomerId
		requestCustomerLogin.Password = request.Password
		requestCustomerLogin.Username = request.Username

		_, err := u.ModelMysqlCustomerLogin.CreateBulk(ctx, &model_mysql_customer_login.CreateBulkRequest{
			Transaction: transactionSql,
			Data: []model_mysql_customer_login.CustomerLoginModeCreate{
				*requestCustomerLogin,
			},
		})
		if err != nil {
			responseChannel <- RegisterChannelInsertLimitAndAuthResponse{
				Error: err,
			}
			return
		}

		responseChannel <- RegisterChannelInsertLimitAndAuthResponse{
			Error: nil,
		}

	}(
		&wg,
		&RegisterChannelInsertLimitAndAuthRequest{CustomerId: customerId},
		responseChan,
	)

	go func(wg *sync.WaitGroup, requestChannel *RegisterChannelInsertLimitAndAuthRequest, responseChannel chan<- RegisterChannelInsertLimitAndAuthResponse) {

		defer wg.Done()

		listRequestCustomerLimit := make([]model_mysql_customer_limit.CustomerLimitModeCreate, 0)
		for _, itemOfCustomerLimit := range request.DataCustomerLimit {
			listRequestCustomerLimit = append(listRequestCustomerLimit, model_mysql_customer_limit.CustomerLimitModeCreate{
				CustomerId: requestChannel.CustomerId,
				Tenor:      itemOfCustomerLimit.Tenor,
				Limit:      itemOfCustomerLimit.Limit,
			})
		}

		_, err := u.ModelMysqlCustomerLimit.CreateBulk(ctx, &model_mysql_customer_limit.CreateBulkRequest{
			Transaction: transactionSql,
			Data:        listRequestCustomerLimit,
		})
		if err != nil {
			responseChannel <- RegisterChannelInsertLimitAndAuthResponse{
				Error: err,
			}
			return
		}

		responseChannel <- RegisterChannelInsertLimitAndAuthResponse{
			Error: nil,
		}

	}(
		&wg,
		&RegisterChannelInsertLimitAndAuthRequest{CustomerId: customerId},
		responseChan,
	)

	wg.Wait()

	indexCounting := 0
	for elem := range responseChan {
		indexCounting++
		if elem.Error != nil {
			transactionSql.Rollback()
			return response, elem.Error
		}
		if indexCounting == targetGoroutines {
			break
		}
	}

	transactionSql.Commit()
	return response, err

}
