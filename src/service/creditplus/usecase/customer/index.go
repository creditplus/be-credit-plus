package usecase_customer

import (
	"context"
	"database/sql"

	pkg_jwt "gitlab.com/creditplus/be-credit-plus/src/pkg/jwt"
	model_mysql_customer "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/customer"
	model_mysql_customer_limit "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/customer_limit"
	model_mysql_customer_login "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/customer_login"
)

type (
	UsecaseCustomer struct {
		PkgJwt                  pkg_jwt.PkgJwtInterface
		ModelMysqlCustomerLogin model_mysql_customer_login.ModelMysqlCustomerLoginInterface
		ModelMysqlCustomer      model_mysql_customer.ModelMysqlCustomerInterface
		ModelMysqlCustomerLimit model_mysql_customer_limit.ModelMysqlCustomerLimitInterface
		ConnectionMysql         *sql.DB
	}

	UsecaseCustomerAuthInterface interface {
		Login(ctx context.Context, request *LoginRequest) (response LoginResponse, err error)
		Register(ctx context.Context, request *RegisterRequest) (response RegisterResponse, err error)
	}

	UsecaseCustomerInterface interface {
		UsecaseCustomerAuthInterface
	}
)

func New(
	pkgJwt pkg_jwt.PkgJwtInterface,
	modelMysqlCustomerLogin model_mysql_customer_login.ModelMysqlCustomerLoginInterface,
	modelMysqlCustomer model_mysql_customer.ModelMysqlCustomerInterface,
	modelMysqlCustomerLimit model_mysql_customer_limit.ModelMysqlCustomerLimitInterface,
	connectionMysql *sql.DB,
) UsecaseCustomerInterface {
	return &UsecaseCustomer{
		PkgJwt:                  pkgJwt,
		ModelMysqlCustomerLogin: modelMysqlCustomerLogin,
		ModelMysqlCustomer:      modelMysqlCustomer,
		ModelMysqlCustomerLimit: modelMysqlCustomerLimit,
		ConnectionMysql:         connectionMysql,
	}
}
