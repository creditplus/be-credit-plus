package usecase_transaction_credit

type (
	Transaction struct {
		Tenor        int     `json:"tenor"`
		Otr          float64 `json:"otr"`
		AdminFee     float64 `json:"admin_fee"`
		TotalCicilan float64 `json:"total_cicilan"`
		TotalBunga   float64 `json:"total_bunga"`
		NameAsset    string  `json:"name_asset"`
	}

	CreateNewTransactionCreditRequest struct {
		CustomerId string
		Tenor      int
		PriceAsset float64
		NameAsset  string
	}

	CreateNewTransactionCreditResponse struct {
		Data []Transaction
	}

	CalculateOtrRequest struct {
		PriceAsset float64
	}

	CalculateOtrResponse struct {
		Otr float64
	}

	CalculateAdminFeeRequest struct {
		PriceAsset float64
	}

	CalculateAdminFeeResponse struct {
		AdminFee float64
	}

	CalculateBungaRequest struct {
		PriceAsset float64
	}

	CalculateBungaResponse struct {
		Bunga float64
	}
)
