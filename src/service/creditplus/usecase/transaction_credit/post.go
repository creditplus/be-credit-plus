package usecase_transaction_credit

import (
	"context"
	"errors"

	model_mysql_customer "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/customer"
	model_mysql_transaction "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/transaction"
)

func (u *UsecaseTransactionCredit) calculateOtr(request *CalculateOtrRequest) (response CalculateOtrResponse) {
	priceOfBPKB := 1_000_000
	priceOfPajak := 250_000

	response.Otr = request.PriceAsset + float64(priceOfBPKB) + float64(priceOfPajak)
	return response
}

func (u *UsecaseTransactionCredit) calculateAdminFee(request *CalculateAdminFeeRequest) (response CalculateAdminFeeResponse) {
	percentageAdminFee := 10
	resultPercentageAdminFee := (request.PriceAsset * float64(percentageAdminFee)) / 100

	response.AdminFee = resultPercentageAdminFee
	return response
}

func (u *UsecaseTransactionCredit) calculateBunga(request *CalculateBungaRequest) (response CalculateBungaResponse) {
	percentageBunga := 2
	resultPercentageBunga := (request.PriceAsset * float64(percentageBunga)) / 100

	response.Bunga = resultPercentageBunga
	return response
}

func (u *UsecaseTransactionCredit) CreateNewTransactionCredit(ctx context.Context, request *CreateNewTransactionCreditRequest) (response CreateNewTransactionCreditResponse, err error) {

	transactionSql, err := u.ConnectionMysql.Begin()
	if err != nil {
		return response, err
	}

	resultModelCustomer, err := u.ModelMysqlCustomer.FindCustomerWithJoinLimit(ctx, &model_mysql_customer.FindCustomerWithJoinLimitRequest{
		Id:          request.CustomerId,
		Transaction: transactionSql,
	})
	if err != nil {
		transactionSql.Rollback()
		return response, err
	}

	mapCustomerWithTenor := make(map[int]model_mysql_customer.CustomerWithLimit, 0)

	for _, itemOfCustomer := range resultModelCustomer.Data {
		if _, found := mapCustomerWithTenor[itemOfCustomer.Tenor]; !found {
			mapCustomerWithTenor[itemOfCustomer.Tenor] = itemOfCustomer
		}
	}

	valueCustomer, foundCustomer := mapCustomerWithTenor[request.Tenor]
	if foundCustomer && valueCustomer.LimitValue < request.PriceAsset {
		transactionSql.Rollback()
		return response, errors.New("transaction rejected - your limit has been reached")
	}

	totalLimitCutting := valueCustomer.LimitValue - request.PriceAsset

	_, err = u.ModelMysqlCustomer.UpdateLimit(ctx, &model_mysql_customer.UpdateLimitRequest{
		CustomerLimitId: valueCustomer.CustomerLimitId,
		Limit:           totalLimitCutting,
		Transaction:     transactionSql,
		Tenor:           request.Tenor,
	})
	if err != nil {
		transactionSql.Rollback()
		return response, err
	}

	resultCalculateOtr := u.calculateOtr(&CalculateOtrRequest{PriceAsset: request.PriceAsset})
	resultCalculateAdminFee := u.calculateAdminFee(&CalculateAdminFeeRequest{PriceAsset: request.PriceAsset})
	resultCalculateBunga := u.calculateBunga(&CalculateBungaRequest{PriceAsset: request.PriceAsset})

	transactionModel := model_mysql_transaction.Transaction{
		CustomerId:   request.CustomerId,
		Tenor:        request.Tenor,
		Otr:          resultCalculateOtr.Otr,
		AdminFee:     resultCalculateAdminFee.AdminFee,
		TotalCicilan: request.PriceAsset,
		TotalBunga:   resultCalculateBunga.Bunga,
		NameAsset:    request.NameAsset,
	}

	_, err = u.ModelMysqlTransaction.CreateBulk(ctx, &model_mysql_transaction.CreateBulkRequest{
		Data: []model_mysql_transaction.Transaction{
			transactionModel,
		},
		Transaction: transactionSql,
	})
	if err != nil {
		transactionSql.Rollback()
		return response, err
	}

	transactionSql.Commit()

	return response, err

}
