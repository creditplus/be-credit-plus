package usecase_transaction_credit

import (
	"context"
	"database/sql"

	model_mysql_customer "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/customer"
	model_mysql_transaction "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/transaction"
)

type (
	UsecaseTransactionCredit struct {
		ModelMysqlCustomer    model_mysql_customer.ModelMysqlCustomerInterface
		ModelMysqlTransaction model_mysql_transaction.ModelMysqlTransactionInterface
		ConnectionMysql       *sql.DB
	}

	UsecaseTransactionCreditPostInterface interface {
		CreateNewTransactionCredit(ctx context.Context, request *CreateNewTransactionCreditRequest) (response CreateNewTransactionCreditResponse, err error)
	}

	UsecaseTransactionCreditInterface interface {
		UsecaseTransactionCreditPostInterface
	}
)

func New(
	modelMysqlCustomer model_mysql_customer.ModelMysqlCustomerInterface,
	modelMysqlTransaction model_mysql_transaction.ModelMysqlTransactionInterface,
	connectionMysql *sql.DB,
) UsecaseTransactionCreditInterface {
	return &UsecaseTransactionCredit{
		ModelMysqlCustomer:    modelMysqlCustomer,
		ModelMysqlTransaction: modelMysqlTransaction,
		ConnectionMysql:       connectionMysql,
	}
}
