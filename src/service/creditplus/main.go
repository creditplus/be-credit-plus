package main

import (
	"log"
	"strconv"

	"github.com/gin-gonic/gin"
	pkg_dotenv "gitlab.com/creditplus/be-credit-plus/src/pkg/dotenv"
	pkg_jwt "gitlab.com/creditplus/be-credit-plus/src/pkg/jwt"
	pkg_mysql "gitlab.com/creditplus/be-credit-plus/src/pkg/mysql"
	config_creditplus "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/config"
	handler_customer "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/handler/customer"
	handler_transaction_credit "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/handler/transaction_credit"
	middleware_creditplus "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/middleware"
	model_mysql_customer "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/customer"
	model_mysql_customer_limit "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/customer_limit"
	model_mysql_customer_login "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/customer_login"
	model_mysql_transaction "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/model/mysql/transaction"
	router_v1_customer "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/router/v1/customer"
	router_v1_transaction_credit "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/router/v1/transaction_credit"
	usecase_customer "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/usecase/customer"
	usecase_transaction_credit "gitlab.com/creditplus/be-credit-plus/src/service/creditplus/usecase/transaction_credit"
)

func main() {

	pkgDotEnv := pkg_dotenv.New()
	if err := pkgDotEnv.Load(".env"); err != nil {
		log.Fatalf("Have some error at load .env file => %s\n", err.Error())
		panic(err)
	}

	convertPort, err := strconv.Atoi(pkgDotEnv.Get("MYSQL_DATABASE_PORT"))
	if err != nil {
		log.Fatalf("Have some error at convert port")
		panic(err)
	}

	convertMinPool, err := strconv.Atoi(pkgDotEnv.Get("MYSQL_DATABASE_MIN_POOL"))
	if err != nil {
		log.Fatalf("Have some error at convert port")
		panic(err)
	}

	convertMaxPool, err := strconv.Atoi(pkgDotEnv.Get("MYSQL_DATABASE_MAX_POOL"))
	if err != nil {
		log.Fatalf("Have some error at convert port")
		panic(err)
	}

	configVariable := new(config_creditplus.ConfigCredtiPlus)
	configVariable.ConfigServicePort = pkgDotEnv.Get("SERVICE_PORT")
	configVariable.ConfigServiceMode = pkgDotEnv.Get("SERVICE_MODE")
	configVariable.ConfigMysqlDatabaseName = pkgDotEnv.Get("MYSQL_DATABASE_NAME")
	configVariable.ConfigMysqlDatabaseServer = pkgDotEnv.Get("MYSQL_DATABASE_SERVER")
	configVariable.ConfigMysqlDatabasePort = convertPort
	configVariable.ConfigMysqlDatabaseUser = pkgDotEnv.Get("MYSQL_DATABASE_USER")
	configVariable.ConfigMysqlDatabasePassword = pkgDotEnv.Get("MYSQL_DATABASE_PASSWORD")
	configVariable.ConfigMysqlDatabaseMinPool = convertMinPool
	configVariable.ConfigMysqlDatabaseMaxPool = convertMaxPool
	configVariable.AuthorizationTokenSecret = pkgDotEnv.Get("AUTHORIZATION_TOKEN_SECRET")

	if configVariable.ConfigServiceMode == "production" {
		gin.SetMode(gin.ReleaseMode)
	}

	pkgMssqlDatabaseTools := pkg_mysql.NewTools(
		configVariable.ConfigMysqlDatabaseName,
		configVariable.ConfigMysqlDatabaseServer,
		configVariable.ConfigMysqlDatabasePort,
		configVariable.ConfigMysqlDatabaseUser,
		configVariable.ConfigMysqlDatabasePassword,
		configVariable.ConfigMysqlDatabaseMinPool,
		configVariable.ConfigMysqlDatabaseMaxPool,
	)
	pkgMysqlDatabaseConnection := pkg_mysql.NewConnection(
		configVariable.ConfigMysqlDatabaseName,
		configVariable.ConfigMysqlDatabaseServer,
		configVariable.ConfigMysqlDatabasePort,
		configVariable.ConfigMysqlDatabaseUser,
		configVariable.ConfigMysqlDatabasePassword,
		configVariable.ConfigMysqlDatabaseMinPool,
		configVariable.ConfigMysqlDatabaseMaxPool,
	)

	connectionMysql, err := pkgMysqlDatabaseConnection.CreateConnection()
	if err != nil {
		log.Fatalf("Have some error at create connection mysql => %s\n", err.Error())
		panic(err)
	}

	defer connectionMysql.Close()

	pkgJwt := pkg_jwt.New(configVariable.AuthorizationTokenSecret)

	modelMysqlCustomer := model_mysql_customer.New(pkgMssqlDatabaseTools, connectionMysql)
	modelMysqlCustomerLimit := model_mysql_customer_limit.New(pkgMssqlDatabaseTools, connectionMysql)
	modelMysqlCustomerLogin := model_mysql_customer_login.New(pkgMssqlDatabaseTools, connectionMysql)
	modelMysqlTransaction := model_mysql_transaction.New(pkgMssqlDatabaseTools, connectionMysql)

	usecaseTransactionCredit := usecase_transaction_credit.New(modelMysqlCustomer, modelMysqlTransaction, connectionMysql)
	usecaseCustomer := usecase_customer.New(pkgJwt, modelMysqlCustomerLogin, modelMysqlCustomer, modelMysqlCustomerLimit, connectionMysql)

	handlerTransactionCredit := handler_transaction_credit.New(usecaseTransactionCredit)
	handlerCustomer := handler_customer.New(usecaseCustomer)

	middlewareCreditPlus := middleware_creditplus.New(pkgJwt)

	r := gin.New()

	r.ForwardedByClientIP = true
	r.SetTrustedProxies([]string{"127.0.0.1"})

	groupAPI := r.Group("/api")
	groupAPIV1 := groupAPI.Group("/v1")

	router_v1_transaction_credit.New(groupAPIV1, middlewareCreditPlus, handlerTransactionCredit)
	router_v1_customer.New(groupAPIV1, middlewareCreditPlus, handlerCustomer)

	r.Run(configVariable.ConfigServicePort)

}
